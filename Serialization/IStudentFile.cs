﻿using System;

namespace Serialization
{
    internal interface IStudentFile
    {
        void Read(ref StudentList lst, String AName);
        void Write(ref StudentList lst, String AName);
    }
}
