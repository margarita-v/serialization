﻿namespace Serialization
{
    partial class FormForAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFio = new System.Windows.Forms.TextBox();
            this.numCourse = new System.Windows.Forms.NumericUpDown();
            this.numGroup = new System.Windows.Forms.NumericUpDown();
            this.cbFormOfStudy = new System.Windows.Forms.ComboBox();
            this.numCountOfExams = new System.Windows.Forms.NumericUpDown();
            this.gvMarks = new System.Windows.Forms.DataGridView();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGroup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCountOfExams)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMarks)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "ФИО";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Курс";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Группа";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 144);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Форма обучения";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 189);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Количество экзаменов";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 218);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Успеваемость";
            // 
            // tbFio
            // 
            this.tbFio.Location = new System.Drawing.Point(192, 27);
            this.tbFio.Name = "tbFio";
            this.tbFio.Size = new System.Drawing.Size(100, 20);
            this.tbFio.TabIndex = 6;
            // 
            // numCourse
            // 
            this.numCourse.Location = new System.Drawing.Point(192, 60);
            this.numCourse.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numCourse.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCourse.Name = "numCourse";
            this.numCourse.Size = new System.Drawing.Size(120, 20);
            this.numCourse.TabIndex = 7;
            this.numCourse.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numGroup
            // 
            this.numGroup.Location = new System.Drawing.Point(192, 104);
            this.numGroup.Maximum = new decimal(new int[] {
            91,
            0,
            0,
            0});
            this.numGroup.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numGroup.Name = "numGroup";
            this.numGroup.Size = new System.Drawing.Size(120, 20);
            this.numGroup.TabIndex = 8;
            this.numGroup.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // cbFormOfStudy
            // 
            this.cbFormOfStudy.FormattingEnabled = true;
            this.cbFormOfStudy.Items.AddRange(new object[] {
            "бюджет",
            "договор"});
            this.cbFormOfStudy.Location = new System.Drawing.Point(191, 141);
            this.cbFormOfStudy.Name = "cbFormOfStudy";
            this.cbFormOfStudy.Size = new System.Drawing.Size(121, 21);
            this.cbFormOfStudy.TabIndex = 9;
            // 
            // numCountOfExams
            // 
            this.numCountOfExams.Location = new System.Drawing.Point(192, 182);
            this.numCountOfExams.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numCountOfExams.Name = "numCountOfExams";
            this.numCountOfExams.Size = new System.Drawing.Size(120, 20);
            this.numCountOfExams.TabIndex = 10;
            this.numCountOfExams.ValueChanged += new System.EventHandler(this.numCountOfExams_ValueChanged);
            // 
            // gvMarks
            // 
            this.gvMarks.AllowUserToAddRows = false;
            this.gvMarks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvMarks.Location = new System.Drawing.Point(15, 249);
            this.gvMarks.Name = "gvMarks";
            this.gvMarks.Size = new System.Drawing.Size(297, 220);
            this.gvMarks.TabIndex = 11;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(30, 478);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 12;
            this.btnOk.Text = "OK";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(217, 478);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 13;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // FormForAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 513);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.gvMarks);
            this.Controls.Add(this.numCountOfExams);
            this.Controls.Add(this.cbFormOfStudy);
            this.Controls.Add(this.numGroup);
            this.Controls.Add(this.numCourse);
            this.Controls.Add(this.tbFio);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormForAdd";
            ((System.ComponentModel.ISupportInitialize)(this.numCourse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGroup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCountOfExams)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvMarks)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.TextBox tbFio;
        public System.Windows.Forms.NumericUpDown numCourse;
        public System.Windows.Forms.NumericUpDown numGroup;
        public System.Windows.Forms.ComboBox cbFormOfStudy;
        public System.Windows.Forms.NumericUpDown numCountOfExams;
        public System.Windows.Forms.DataGridView gvMarks;
    }
}