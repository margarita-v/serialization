﻿using System;
using System.Windows.Forms;

namespace Serialization
{
    public partial class FormForAdd : Form
    {
        public bool ok = false;
        public FormForAdd()
        {
            InitializeComponent();
            gvMarks.ColumnCount = 2;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ok = false;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (tbFio.Text != null && numCountOfExams.Value != 0 && cbFormOfStudy.SelectedIndex != null)
            {
                ok = true;
                Close();
            }
            else
                MessageBox.Show("Вы заполнили не все обязательные поля.");
        }

        private void numCountOfExams_ValueChanged(object sender, EventArgs e)
        {
            gvMarks.RowCount = Convert.ToInt32(numCountOfExams.Value) * 2 * Convert.ToInt32(numCourse.Value) + 1;
            gvMarks.Rows[0].Cells[0].Value = "Название предмета";
            gvMarks.Rows[0].Cells[0].ReadOnly = true;
            gvMarks.Rows[0].Cells[1].Value = "Оценка";
            gvMarks.Rows[0].Cells[1].ReadOnly = true;
        }
    }
}
