﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Serialization
{
    public partial class FormTask : Form
    {
        StudentList lst = new StudentList();

        public FormTask(StudentList pList)
        {
            InitializeComponent();
            lst = pList;
        }

        private void btnSaveAs_Click(object sender, EventArgs e)
        {
            FileInfo finf;
            IStudentFile studentFile;
            string filename = null;
            saveFileDialog1.Filter = "Файлы Excel (*.txt; *.dat;*.xml)|*.txt; *.dat;*.xml";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != null)
            {
                filename = saveFileDialog1.FileName;
                finf = new FileInfo(filename);
                switch (finf.Extension)
                {
                    case ".txt":
                        studentFile = new StudentTextFile();
                        break;

                    case ".dat":
                        studentFile = new StudentBinFile();
                        break;

                    case ".xml":
                        studentFile = new StudentXmlFile();
                        break;

                    default:
                        studentFile = new StudentTextFile();
                        break;
                }
                studentFile.Write(ref lst, filename);  //записываем данные в файл
            }
        }
    }
}
