﻿using System;
using System.Windows.Forms;

namespace Serialization
{
    public partial class FormForDelete : Form
    {
        public int ChosenNumber;
        public FormForDelete(int maximum)
        {
            InitializeComponent();
            numOrderNumber.Maximum = maximum;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            ChosenNumber = Convert.ToInt32(numOrderNumber.Value);
            Close();
        }
    }
}
