﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Serialization
{
    public partial class FormMain : Form
    {
        StudentList lst = new StudentList();
        Student tmpstudent = new Student();
        string filename = null;
        FileInfo finf;
        IStudentFile studentFile;  //объект, с помощью которого будем читать данные и писать их в файлы

        public FormMain()
        {
            InitializeComponent();
            Save.Enabled = false;
            SaveAs.Enabled = false;
            Delete.Enabled = false;
            Edit.Enabled = false;

        }

        private void Open_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Файлы Excel (*.txt; *.dat;*.xml)|*.txt; *.dat;*.xml";
            openFileDialog1.ShowDialog();
            if (openFileDialog1.FileName != null)
            {
                filename = openFileDialog1.FileName;
                finf = new FileInfo(filename);
                switch (finf.Extension)
                {
                    case ".txt":
                        studentFile = new StudentTextFile();
                        break;

                    case ".dat":
                        studentFile = new StudentBinFile();
                        break;

                    case ".xml":
                        studentFile = new StudentXmlFile();
                        break;

                    default:
                        studentFile = new StudentTextFile();
                        break;
                }
                studentFile.Read(ref lst, filename);  //считываем данные из файла
                //lst.PrintToGV(gvStudentTable);
                PrintStudentsInfo.PrintToGV(lst, gvStudentTable);
                Save.Enabled = true;
                SaveAs.Enabled = true;
                Delete.Enabled = true;
                Edit.Enabled = true;
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            if (filename != null)
            {
                studentFile.Write(ref lst, filename);
            }
            else
                SaveAs_Click(sender, e);
        }

        private void SaveAs_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Файлы Excel (*.txt; *.dat;*.xml)|*.txt; *.dat;*.xml";
            saveFileDialog1.ShowDialog();
            if (saveFileDialog1.FileName != null)
            {
                filename = saveFileDialog1.FileName;
                finf = new FileInfo(filename);
                switch (finf.Extension)
                {
                    case ".txt":
                        studentFile = new StudentTextFile();
                        break;

                    case ".dat":
                        studentFile = new StudentBinFile();
                        break;

                    case ".xml":
                        studentFile = new StudentXmlFile();
                        break;

                    default:
                        studentFile = new StudentTextFile();
                        break;
                }
                studentFile.Write(ref lst, filename);  //записываем данные в файл
            }
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Add_Click(object sender, EventArgs e)
        {
            FormForAdd formforadd = new FormForAdd();
            formforadd.ShowDialog();
            if (formforadd.ok)
            {
                tmpstudent.AddStudent(formforadd);
                lst.AddStudent(tmpstudent);
                //lst.PrintToGV(gvStudentTable);
                PrintStudentsInfo.PrintToGV(lst, gvStudentTable);

                Save.Enabled = true;
                SaveAs.Enabled = true;
                Edit.Enabled = true;
            }  
        }

        private void Edit_Click(object sender, EventArgs e)
        {
            FormInputStudentNumber formedit = new FormInputStudentNumber(lst.ListStudent.Count, "Введите номер п/п студента");
            formedit.ShowDialog();
            if (formedit.StudentNumber != 0)
            {
                FormForAdd formforadd = new FormForAdd();
                //formforadd.FillTable(lst.ListStudent[formedit.StudentNumber - 1]);
                lst.ListStudent[formedit.StudentNumber - 1].FillTable(formforadd);
                formforadd.ShowDialog();
                if (formforadd.ok)
                {
                    tmpstudent.EditStudent(formforadd);
                    lst.EditStudent(tmpstudent, formedit.StudentNumber);
                    //lst.PrintToGV(gvStudentTable);
                    PrintStudentsInfo.PrintToGV(lst, gvStudentTable);
                } 
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (lst.ListStudent.Count != 0)
            {
                FormForDelete formfordelete = new FormForDelete(lst.ListStudent.Count);
                formfordelete.ShowDialog();
                lst.ListStudent.RemoveAt(formfordelete.ChosenNumber - 1);
                //lst.PrintToGV(gvStudentTable);
                PrintStudentsInfo.PrintToGV(lst, gvStudentTable);
            }
        }

        private void ShowTask_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Создайте файл Студент. Из файла Студент занести в отдельный файл записи об отличниках.");
        }

        private void Run_Click(object sender, EventArgs e)
        {
            StudentList res = new StudentList();
            for (int i = 0; i < lst.ListStudent.Count; i++)
                if (lst.ListStudent[i].IsExcellent())
                    res.ListStudent.Add(lst.ListStudent[i]);
            if (res.ListStudent.Count != 0)
            {
                FormTask Result = new FormTask(res);
                //res.PrintToGV(Result.gvStudTable);
                PrintStudentsInfo.PrintToGV(res, Result.gvStudTable);
                //Result.FillTable();
                Result.Show();
            }
            else
                MessageBox.Show("Среди студентов нет отличников.");
        }

        private void ShowMarks_Click(object sender, EventArgs e)
        {
            FormInputStudentNumber formmarks = new FormInputStudentNumber(lst.ListStudent.Count, "Введите номер п/п студента");
            formmarks.ShowDialog();
            if (formmarks.StudentNumber != 0)
            {
                Table TableOfMarks = new Table();
                tmpstudent = lst.ListStudent[formmarks.StudentNumber - 1];
                tmpstudent.FillTableOfMarks(TableOfMarks.gvTable);
                TableOfMarks.Show();
            }
        }
    }
}
