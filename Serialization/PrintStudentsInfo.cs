﻿using System.Windows.Forms;

namespace Serialization
{
    internal class PrintStudentsInfo
    {
        public static void PrintToGV(StudentList lst, DataGridView gv)
        {
            gv.ColumnCount = 5;
            gv.RowCount = lst.ListStudent.Count + 1;
            gv.Rows[0].Cells[0].Value = "Номер п/п";
            gv.Rows[0].Cells[1].Value = "ФИО";
            gv.Rows[0].Cells[2].Value = "Курс";
            gv.Rows[0].Cells[3].Value = "Группа";
            gv.Rows[0].Cells[4].Value = "Форма обучения";
            gv.AutoResizeColumns();
            for (int i = 1; i < gv.RowCount; i++)
            {
                gv.Rows[i].Cells[0].Value = i;
                gv.Rows[i].Cells[1].Value = lst.ListStudent[i - 1].FIO;
                gv.Rows[i].Cells[2].Value = lst.ListStudent[i - 1].Course;
                gv.Rows[i].Cells[3].Value = lst.ListStudent[i - 1].Group;
                if (lst.ListStudent[i - 1].FormOfStudy)
                    gv.Rows[i].Cells[4].Value = "бюджет";
                else
                    gv.Rows[i].Cells[4].Value = "договор";
                gv.AutoResizeColumns();
            }
            gv.ReadOnly = true;
        }
    }
}
