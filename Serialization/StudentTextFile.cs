﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Serialization
{
    internal class StudentTextFile: IStudentFile
    {
        private void ReadExam(string str, ref Exam exam)
        {
            int i = 0;
            string subject = null;
            while ((i < str.Length) && (str[i] != ' '))
            {
                subject = subject + str[i];
                i++;
            }
            exam.Subject = subject;
            exam.Mark = Convert.ToInt32(str[i + 1].ToString());

        }
        #region IStudentFile Members

        public void Read(ref StudentList lst, String FName)  //чтение из файла
        {
            String line;
            //Encoding enc = Encoding.GetEncoding(1251);
            StreamReader Sr = new StreamReader(FName, Encoding.UTF8);
            line = Sr.ReadLine();  //считываем число записей
            lst.ListStudent.Clear();
            int n = Convert.ToInt32(line);
            for (int i = 0; i < n; i++)  //считываем каждую запись в список
            {
                Student rec = new Student();
                rec.FIO = Sr.ReadLine();
                rec.Course = Convert.ToInt32(Sr.ReadLine());
                rec.Group = Convert.ToInt32(Sr.ReadLine());
                if (Sr.ReadLine() == "бюджет")
                    rec.FormOfStudy = true;
                else
                    rec.FormOfStudy = false;
                rec.CountOfExams = Convert.ToInt32(Sr.ReadLine());
                for (int j = 0; j < rec.CountOfExams * 2 * rec.Course; j++)
                    ReadExam(Sr.ReadLine(), ref rec.marks[j]);
                lst.ListStudent.Add(rec);
                Sr.ReadLine();
            }
            Sr.Close();  //закрываем файл
        }

        public void Write(ref StudentList lst, String FName)  //запись в файл
        {
            StreamWriter Sw = new StreamWriter(FName + ".txt");
            Sw.WriteLine(lst.ListStudent.Count);
            int n = lst.ListStudent.Count;
            List<Student>.Enumerator num = lst.ListStudent.GetEnumerator();  // получаем итератор списка
            num.MoveNext();
            for (int i = 0; i < n; i++)  // поочередно записываем данные
            {
                Sw.WriteLine(num.Current.FIO);
                Sw.WriteLine(num.Current.Course);
                Sw.WriteLine(num.Current.Group);
                if (num.Current.FormOfStudy)
                    Sw.WriteLine("бюджет");
                else
                    Sw.WriteLine("договор");
                Sw.WriteLine(num.Current.CountOfExams);
                for (int j = 0; j < 2 * num.Current.CountOfExams * num.Current.Course; j++)
                {
                    Sw.WriteLine(num.Current.marks[j].GetString());
                }
                Sw.WriteLine();
                num.MoveNext();
            }
            Sw.Close();
        }
        #endregion
    }
}
