﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Serialization
{
    internal class StudentBinFile: IStudentFile
    {
        #region IStudentFile Members

        public void Read(ref StudentList lst, string AName)
        {
            FileStream fs = new FileStream(AName, FileMode.Open);
            BinaryFormatter formatter = new BinaryFormatter();  //считываем данные в список с помощью бинарного форматтера
            lst.ListStudent.Clear();
            lst = (StudentList)formatter.Deserialize(fs);
            fs.Close();
        }

        public void Write(ref StudentList lst, string AName)
        {
            FileStream fs = new FileStream(AName + ".dat", FileMode.Create);
            BinaryFormatter formatter = new BinaryFormatter();  //записываем данные из списка с помощью бинарного форматтера
            formatter.Serialize(fs, lst);
            fs.Close();
        }

        #endregion
    }
}
