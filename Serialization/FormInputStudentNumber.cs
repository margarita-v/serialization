﻿using System;
using System.Windows.Forms;

namespace Serialization
{
    public partial class FormInputStudentNumber : Form
    {
        public int StudentNumber;
        public FormInputStudentNumber(int maximum, string str)
        {
            InitializeComponent();
            numStudentNumber.Maximum = maximum;
            label1.Text = str;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            StudentNumber = 0;
            Close();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            StudentNumber = Convert.ToInt32(numStudentNumber.Value);
            Close();
        }
    }
}
