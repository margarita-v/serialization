﻿namespace Serialization
{
    partial class FormTask
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gvStudTable = new System.Windows.Forms.DataGridView();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnSaveAs = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gvStudTable)).BeginInit();
            this.SuspendLayout();
            // 
            // gvStudTable
            // 
            this.gvStudTable.AllowUserToAddRows = false;
            this.gvStudTable.AllowUserToDeleteRows = false;
            this.gvStudTable.AllowUserToOrderColumns = true;
            this.gvStudTable.AllowUserToResizeColumns = false;
            this.gvStudTable.AllowUserToResizeRows = false;
            this.gvStudTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvStudTable.ColumnHeadersVisible = false;
            this.gvStudTable.Location = new System.Drawing.Point(2, 2);
            this.gvStudTable.Name = "gvStudTable";
            this.gvStudTable.ReadOnly = true;
            this.gvStudTable.RowHeadersVisible = false;
            this.gvStudTable.Size = new System.Drawing.Size(580, 254);
            this.gvStudTable.TabIndex = 0;
            // 
            // btnSaveAs
            // 
            this.btnSaveAs.Location = new System.Drawing.Point(220, 271);
            this.btnSaveAs.Name = "btnSaveAs";
            this.btnSaveAs.Size = new System.Drawing.Size(126, 34);
            this.btnSaveAs.TabIndex = 1;
            this.btnSaveAs.Text = "Сохранить как";
            this.btnSaveAs.UseVisualStyleBackColor = true;
            this.btnSaveAs.Click += new System.EventHandler(this.btnSaveAs_Click);
            // 
            // FormTask
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 317);
            this.Controls.Add(this.btnSaveAs);
            this.Controls.Add(this.gvStudTable);
            this.Name = "FormTask";
            ((System.ComponentModel.ISupportInitialize)(this.gvStudTable)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnSaveAs;
        public System.Windows.Forms.DataGridView gvStudTable;
    }
}