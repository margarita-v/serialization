﻿using System;
using System.Windows.Forms;

namespace Serialization
{
    [Serializable]
    public struct Exam
    {
        public string Subject;
        public int Mark;
        public string GetString()
        {
            return Subject + " " + Mark.ToString();
        }
    }
    [Serializable]
    public class Student
    {
        public string FIO;
        public int Course;
        public int Group;
        public bool FormOfStudy; // true - бюджет, false - договор
        public int CountOfExams;
        public Exam[] marks = new Exam[50];
        public bool IsExcellent()
        {
            bool ok = true;
            for (int j = 0; j < CountOfExams * 2 * Course && ok; j++)
                ok = marks[j].Mark == 5; 
            return ok;
        }

        public void AddStudent(FormForAdd formforadd)
        {
            FIO = formforadd.tbFio.Text;
            Course = Convert.ToInt32(formforadd.numCourse.Value);
            Group = Convert.ToInt32(formforadd.numGroup.Value);
            if (formforadd.cbFormOfStudy.SelectedIndex == 0)
                FormOfStudy = true;
            else
                FormOfStudy = false;
            CountOfExams = Convert.ToInt32(formforadd.numCountOfExams.Value);
            for (int i = 1; i < CountOfExams * 2 * Course + 1; i++) //
            {
                marks[i - 1].Subject = formforadd.gvMarks.Rows[i].Cells[0].Value.ToString();
                marks[i - 1].Mark = Convert.ToInt32(formforadd.gvMarks.Rows[i].Cells[1].Value);
            }
        }

        public void EditStudent(FormForAdd formforadd)
        {
            FIO = formforadd.tbFio.Text;
            Course = Convert.ToInt32(formforadd.numCourse.Value);
            Group = Convert.ToInt32(formforadd.numGroup.Value);
            if (formforadd.cbFormOfStudy.SelectedIndex == 0)
                FormOfStudy = true;
            else
                FormOfStudy = false;
            CountOfExams = Convert.ToInt32(formforadd.numCountOfExams.Value);
            for (int i = 1; i < CountOfExams * 2 * Course + 1; i++)
            {
                marks[i - 1].Subject = formforadd.gvMarks.Rows[i].Cells[0].Value.ToString();
                marks[i - 1].Mark = Convert.ToInt32(formforadd.gvMarks.Rows[i].Cells[1].Value);
            }
        }

        public void FillTable(FormForAdd formforadd)
        {
            formforadd.tbFio.Text = FIO;
            formforadd.numCourse.Value = Course;
            formforadd.numGroup.Value = Group;
            if (FormOfStudy)
                formforadd.cbFormOfStudy.SelectedIndex = 0;
            else
                formforadd.cbFormOfStudy.SelectedIndex = 1;
            formforadd.numCountOfExams.Value = CountOfExams; 
            for (int i = 1; i < CountOfExams * 2 * Course + 1; i++)
            {
                formforadd.gvMarks.Rows[i].Cells[0].Value = marks[i - 1].Subject;
                formforadd.gvMarks.Rows[i].Cells[1].Value = marks[i - 1].Mark;
            }
            //gvMarks.ReadOnly = true;
        }

        public void FillTableOfMarks(DataGridView gvMark)
        {
            gvMark.ColumnCount = 2;
            gvMark.RowCount = CountOfExams * 2 * Course + 1;
            gvMark.Rows[0].Cells[0].Value = "Название предмета";
            gvMark.Rows[0].Cells[1].Value = "Оценка";
            for (int i = 1; i < gvMark.RowCount; i++)  
            {
                gvMark.Rows[i].Cells[0].Value = marks[i - 1].Subject;
                gvMark.Rows[i].Cells[1].Value = marks[i - 1].Mark;
            }
        }
    }
}
