﻿using System.IO;
using System.Xml.Serialization;

namespace Serialization
{
    internal class StudentXmlFile: IStudentFile
    {
        #region IStudentFile Members

        public void Read(ref StudentList lst, string AName)
        {
            FileStream fs = new FileStream(AName, FileMode.Open);
            XmlSerializer xs = new XmlSerializer(typeof(StudentList));  //считываем данные в список с помощью сериалайзера
            lst.ListStudent.Clear();
            lst = (StudentList)xs.Deserialize(fs);
            fs.Close();
        }

        public void Write(ref StudentList lst, string AName)
        {
            FileStream fs = new FileStream(AName + ".xml", FileMode.Create);
            XmlSerializer xs = new XmlSerializer(typeof(StudentList));  //запиываем данные из списка с помощью сериалайзера
            xs.Serialize(fs, lst);
            fs.Close();
        }

        #endregion
    }
}
